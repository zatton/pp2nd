#include "nn.h"
#include <time.h>

void print(int m, int n, const float * x){
  int i,j;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      printf("%.4f ",x[n*i+j]);
    }
    putchar('\n');
  }
}
void copy(int n,const float *x,float *y){
  int i;
  for(i=0;i<n;i++){
    y[i]=x[i];
  }
}
void fc(int m,int n,const float *x,const float *A,const float *b,float *y){
  //y=Ax+b
  int i,j;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      y[i]+=A[i*n+j]*x[j];
    }
    y[i]+=b[i];
  }
}
void relu(int n,const float *x,float *y){
  //remove x<=0
  int i;
  for(i=0;i<n;i++){
    if(x[i]>0){
      y[i]=x[i];
    }else{
      y[i]=0;
    }
  }
}
void softmax(int n,const float *x,float *y){
  int i;
  float x_max=0;
  float exp_sum=0;

  for(i=0;i<n;i++){
    x_max=x[i]>x_max?x[i]:x_max;
  }
  for(i=0;i<n;i++){
    exp_sum+=exp(x[i]-x_max);
  }
  for(i=0;i<n;i++){
    y[i]=exp(x[i]-x_max)/exp_sum;
  }
}
int inference3(const float *A,const float *b,const float *x,float *y){
  float max=0.0;
  int i,ans;

  fc(10,784,x,A,b,y);
  relu(10,y,y);
  softmax(10,y,y);

  for(i=0;i<10;i++){
    if(y[i]>max){
      max=y[i];
      ans=i;
    }
  }
  return ans;
}
void softmaxwithloss_bwd(int n, const float *y,unsigned char t,float *dx){
  copy(10,y,dx);
  dx[t]-=1;
}
void relu_bwd(int n,const float *x,const float *dy,float *dx){
  int i;
  for(i=0;i<n;i++){
    if(x[i]>0){
      dx[i]=dy[i];
    }else{
      dx[i]=0;
    }
  }
}
void fc_bwd(int m,int n,const float *x,const float *dy,const float *A,float *dA,float *db,float *dx){
  int i,j;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      dA[n*i+j]=dy[i]*x[j];
    }
  }

  //for(i=0;i<m;i++){
  //  db[i]=dy[i];
  //}
  copy(m,dy,db);

  for(i=0;i<n;i++){
    for(j=0;j<m;j++){
      dx[i]+=A[n*j+i]*dy[j];
    }
  }
}
void backward3(const float *A,const float *b,const float *x,unsigned char t,float *y,float *dA,float *db){
  float *x_fc   =malloc(sizeof(float)*784);
  float *x_relu =malloc(sizeof(float)*10);
  float *dx_soft=malloc(sizeof(float)*10);
  float *dx_relu=malloc(sizeof(float)*10);
  float *dx_fc  =malloc(sizeof(float)*784);

  copy(784,x,x_fc);
  fc(10,784,x,A,b,y);

  copy(10,y,x_relu);
  relu(10,y,y);

  softmax(10,y,y);

  softmaxwithloss_bwd(10,y,t,dx_soft);
  relu_bwd(10,x_relu,dx_soft,dx_relu);
  fc_bwd(10,784,x_fc,dx_relu,A,dA,db,dx_fc);
}
void shuffle(int n,unsigned int *x){
  int i,j,x_i;
  for(i=0;i<n;i++){
    j=(int)(rand()*(float)n/(1.0+RAND_MAX));
    x_i=x[i]; //一時的に保存
    x[i]=x[j];
    x[j]=x_i;
  }
}
float loss(const float y, int t){
  return -1*t*log(y+0.0000001);
}
void add(int n,const float *x,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]+=x[i];
  }
}
void scale(int n,float x,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]*=x;
  }
}
void init(int n,float x,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]=x;
  }
}
void rand_init(int n,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]=-1.0+rand()*2.0/(1.0+RAND_MAX);
;
  }
}
void save(const char *filename,int m,int n,const float *A,const float *b){

}
void load(const char *filename,int m,int n,float *A, float *b){

}
int inference6( 
    const float *A1,const float *b1,
    const float *A2,const float *b2,
    const float *A3,const float *b3,
    const float *x,float *y){

  int i,ans;
  float max=0.0;

  float *y_50 =malloc(sizeof(float)*50);
  float *y_100=malloc(sizeof(float)*100);

  fc(50,784,x,A1,b1,y_50);
  relu(50,y_50,y_50);
  fc(100,50,y_50,A2,b2,y_100);
  relu(100,y_100,y_100);
  fc(10,100,y_100,A3,b3,y);
  softmax(10,y,y);
  
  for(i=0;i<10;i++){
    if(y[i]>max){
      max=y[i];
      ans=i;
    }
  }
  return ans;
}

int main() {
  float * train_x = NULL;
  unsigned char * train_y = NULL;
  int train_count = -1;

  float * test_x = NULL;
  unsigned char * test_y = NULL;
  int test_count = -1;

  int width = -1;
  int height = -1;

  load_mnist(&train_x, &train_y, &train_count,
             &test_x, &test_y, &test_count,
             &width, &height);

  // これ以降，３層NNの係数 A_784x10 および b_784x10 と，
  // 訓練データ train_x[0]～train_x[train_count-1], train_y[0]～train_x[train_count-1],
  // テストデータ test_x[0]～test_x[test_count-1], test_y[0]～test_y[test_count-1],
  // を使用することができる．
  
  //rand関数初期化
  srand((unsigned int)time(NULL));
  rand();rand();rand();rand();rand();

  //print(1,10,b_784x10);

  //float *y=malloc(sizeof(float)*10);
  //fc(10,784,train_x,A_784x10,b_784x10,y);
  //relu(10,y,y);
  //softmax(10,y,y);
  //print(1,10,y);

  //int ans=inference3(A_784x10,b_784x10,train_x,y);
  //printf("%d %d\n",ans,train_y[0]);

  //int i=9;
  //save_mnist_bmp(train_x+784*i,"train_%05d.bmp",i);

  //int i;
  //int sum=0;
  //for(i=0;i<test_count;i++){
  //  if(inference3(A_784x10,b_784x10,test_x+i*width*height,y)==test_y[i]){
  //    sum++;
  //  }
  //}
  //printf("%f%%\n",sum*100.0/test_count);

  //print(28,28,train_x+784*9);

  //float *y  =malloc(sizeof(float)*10);
  //float *dA =malloc(sizeof(float)*784*10);
  //float *db =malloc(sizeof(float)*10);
  //backward3(A_784x10,b_784x10,train_x+784*8,train_y[8],y,dA,db);
  //print(784,10,dA);
  //print(1,10,db);

  //printf("train_count is %d",train_count);
  
  //int *index=malloc(sizeof(int)*10);
  //for(i=0;i<10;i++){
  //  index[i]=i;
  //}
  //shuffle(10,index);

  //NN3
  int i,j,k,l;
  int epoc=30;
  int batch_size=100;
  int batch_count=train_count/batch_size;
  float learning_rate;
  int sum;
  float e;

  float *A      =malloc(sizeof(float)*784*10);
  float *b      =malloc(sizeof(float)*10);
  float *y      =malloc(sizeof(float)*10);
  unsigned int *index    =malloc(sizeof(int)*train_count);
  float *dA      =malloc(sizeof(float)*784*10);
  float *db      =malloc(sizeof(float)*10);
  float *dA_ave =malloc(sizeof(float)*784*10);
  float *db_ave =malloc(sizeof(float)*10);

  printf("learning_rate:"); scanf("%f",&learning_rate);
  
  for(i=0;i<train_count;i++){
    index[i]=i;
  }
  
  rand_init(784*10,A);
  rand_init(10,b);

  for(i=0;i<epoc;i++){
    shuffle(train_count,index);
    for(j=0;j<batch_count;j++){
      init(784*10,0,dA_ave);
      init(10,0,db_ave);
      for(k=0;k<batch_size;k++){
        backward3(A,b,train_x+width*height*index[j*batch_size+k],train_y[index[j*batch_size+k]],y,dA,db);
        add(784*10,dA,dA_ave);
        add(10,db,db_ave);
      }
      scale(784*10,-1*learning_rate/batch_size,dA_ave);
      scale(10,-1*learning_rate/batch_size,db_ave);
      add(784*10,dA_ave,A);
      add(10,db_ave,b);
    }
    sum=0;
    e=0.0;
    for(l=0;l<test_count;l++){
      if(inference3(A,b,test_x+l*width*height,y)==test_y[l]){
        sum++;
      }
      e+=loss(y[test_y[l]],1);
    }
    printf("%d回目:%.2f%% loss:%.2f\n",i+1,sum*100.0/test_count,e/test_count);
  }
  
  return 0;
}

