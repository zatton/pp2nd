#include "nn.h"
#include <time.h>

typedef struct {
  float A1[784*50];
  float b1[50];
  float A2[50*100];
  float b2[100];
  float A3[100*10];
  float b3[10];
} Params6;

void print(int m, int n, const float * x){
  int i,j;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      printf("%.4f ",x[n*i+j]);
    }
    putchar('\n');
  }
}
void copy(int n,const float *x,float *y){
  int i;
  for(i=0;i<n;i++){
    y[i]=x[i];
  }
}
void fc(int m,int n,const float *x,const float *A,const float *b,float *y){
  //y=Ax+b
  int i,j;
  for(i=0;i<m;i++){
    y[i]=0.0;
    for(j=0;j<n;j++){
      y[i]+=A[i*n+j]*x[j];
    }
    y[i]+=b[i];
  }
}
void relu(int n,const float *x,float *y){
  //remove x<=0
  int i;
  for(i=0;i<n;i++){
    if(x[i]>0){
      y[i]=x[i];
    }else{
      y[i]=0;
    }
  }
}
void softmax(int n,const float *x,float *y){
  int i;
  float x_max=0.0;
  float exp_sum=0.0;

  for(i=0;i<n;i++){
    x_max=x[i]>x_max?x[i]:x_max;
  }
  for(i=0;i<n;i++){
    exp_sum+=exp(x[i]-x_max);
  }
  for(i=0;i<n;i++){
    y[i]=exp(x[i]-x_max)/exp_sum;
  }
}
int inference3(const float *A,const float *b,const float *x,float *y){
  float max=0.0;
  int i,ans;

  fc(10,784,x,A,b,y);
  relu(10,y,y);
  softmax(10,y,y);

  for(i=0;i<10;i++){
    if(y[i]>max){
      max=y[i];
      ans=i;
    }
  }
  return ans;
}
int inference6( 
    const float *A1,const float *b1,
    const float *A2,const float *b2,
    const float *A3,const float *b3,
    const float *x,float *y){

  int i,ans;
  float max=0.0;

  float *y_50 =(float *)malloc(sizeof(float)*50);
  float *y_100=(float *)malloc(sizeof(float)*100);

  fc(50,784,x,A1,b1,y_50);
  relu(50,y_50,y_50);
  fc(100,50,y_50,A2,b2,y_100);
  relu(100,y_100,y_100);
  fc(10,100,y_100,A3,b3,y);
  softmax(10,y,y);
  
  for(i=0;i<10;i++){
    if(y[i]>max){
      max=y[i];
      ans=i;
    }
  }
  return ans;
}
void softmaxwithloss_bwd(int n, const float *y,unsigned char t,float *dx){
  copy(n,y,dx);
  dx[t]-=1;
}
void relu_bwd(int n,const float *x,const float *dy,float *dx){
  int i;
  for(i=0;i<n;i++){
    if(x[i]>0){
      dx[i]=dy[i];
    }else{
      dx[i]=0;
    }
  }
}
void fc_bwd(int m,int n,const float *x,const float *dy,const float *A,float *dA,float *db,float *dx){
  int i,j;
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      dA[n*i+j]=dy[i]*x[j];
    }
  }

  copy(10,dy,db);

  for(i=0;i<n;i++){
    dx[i]=0;
    for(j=0;j<m;j++){
      dx[i]+=A[n*j+i]*dy[j];
    }
  }
}
void backward3(const float *A,const float *b,const float *x,unsigned char t,float *y,float *dA,float *db){
  float *x_fc   =(float *)malloc(sizeof(float)*784);
  float *x_relu =(float *)malloc(sizeof(float)*10);
  float *dx_soft=(float *)malloc(sizeof(float)*10);
  float *dx_relu=(float *)malloc(sizeof(float)*10);
  float *dx_fc  =(float *)malloc(sizeof(float)*784);

  copy(784,x,x_fc);
  fc(10,784,x_fc,A,b,x_relu);
  relu(10,x_relu,y);

  softmax(10,y,y);

  softmaxwithloss_bwd(10,y,t,dx_soft);
  relu_bwd(10,x_relu,dx_soft,dx_relu);
  fc_bwd(10,784,x_fc,dx_relu,A,dA,db,dx_fc);
}
void backward6(
    const Params6 *params,
    const float *x,unsigned char t,
    Params6 *d_params
    ){
  float *x_fc1    =(float *)malloc(sizeof(float)*784); 
  float *x_relu1  =(float *)malloc(sizeof(float)*50); 
  float *x_fc2    =(float *)malloc(sizeof(float)*50); 
  float *x_relu2  =(float *)malloc(sizeof(float)*100); 
  float *x_fc3    =(float *)malloc(sizeof(float)*100); 

  float *dx_fc1   =(float *)malloc(sizeof(float)*784); 
  float *dx_relu1 =(float *)malloc(sizeof(float)*50); 
  float *dx_fc2   =(float *)malloc(sizeof(float)*50); 
  float *dx_relu2 =(float *)malloc(sizeof(float)*100); 
  float *dx_fc3   =(float *)malloc(sizeof(float)*100); 
  float *dx_soft  =(float *)malloc(sizeof(float)*10); 

  float *y        =(float *)malloc(sizeof(float)*10); 

  copy(784,x,x_fc1);
  fc(50,784,x_fc1,params->A1,params->b1,x_relu1);
  relu(50,x_relu1,x_fc2);
  fc(100,50,x_fc2,params->A2,params->b2,x_relu2);
  relu(100,x_relu2,x_fc3);
  fc(10,100,x_fc3,params->A3,params->b3,y);
  softmax(10,y,y);

  softmaxwithloss_bwd(10,y,t,dx_soft);
  fc_bwd(10,100,x_fc3,dx_soft,params->A3,d_params->A3,d_params->b3,dx_fc3);
  relu_bwd(100,x_relu2,dx_fc3,dx_relu2);
  fc_bwd(100,50,x_fc2,dx_relu2,params->A2,d_params->A2,d_params->b2,dx_fc2);
  relu_bwd(50,x_relu1,dx_fc2,dx_relu1);
  fc_bwd(50,784,x_fc1,dx_relu1,params->A1,d_params->A1,d_params->b1,dx_fc1);

  free(x_fc1);
  free(x_relu1);
  free(x_fc2);
  free(x_relu2);
  free(x_fc3);
  
  free(dx_fc1);
  free(dx_relu1);
  free(dx_fc2);
  free(dx_relu2);
  free(dx_fc3);
  free(dx_soft);

}
void shuffle(int n, int *x){
  int i,j,x_i;
  for(i=0;i<n;i++){
    j=(int)(rand()*(float)n/(1.0+RAND_MAX));
    x_i=x[i]; //一時的に保存
    x[i]=x[j];
    x[j]=x_i;
  }
}
float loss(const float y, unsigned char t){
  return -1*t*log(y+0.0000001);
}
void add(int n,const float *x,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]+=x[i];
  }
  for(i=0;i<n;i++){
    o[i]+=x[i];
  }
}
void add6(const Params6 *x_params,Params6 *o_params){
  add(784*50 ,x_params->A1,o_params->A1);
  add(50     ,x_params->b1,o_params->b1);
  add(50*100 ,x_params->A2,o_params->A2);
  add(100    ,x_params->b2,o_params->b2);
  add(100*10 ,x_params->A3,o_params->A3);
  add(10     ,x_params->b3,o_params->b3);
}
void scale(int n,float x,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]*=x;
  }
}
void scale6(float x,Params6 *o_params){
  scale(784*50 ,x,o_params->A1);
  scale(50     ,x,o_params->b1);
  scale(50*100 ,x,o_params->A2);
  scale(100    ,x,o_params->b2);
  scale(100*10 ,x,o_params->A3);
  scale(10     ,x,o_params->b3);
}
void init(int n,float x,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]=x;
  }
}
void init6(Params6 *params){
  init(784*50 ,0,params->A1);
  init(50     ,0,params->b1);
  init(50*100 ,0,params->A2);
  init(100    ,0,params->b2);
  init(100*10 ,0,params->A3);
  init(10     ,0,params->b3);
}
void rand_init(int n,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]=-1.0+rand()*2.0/(1.0+RAND_MAX);
;
  }
}
void rand_init6(Params6 *params){
  rand_init(784*50 ,params->A1);
  rand_init(50     ,params->b1);
  rand_init(50*100 ,params->A2);
  rand_init(100    ,params->b2);
  rand_init(100*10 ,params->A3);
  rand_init(10     ,params->b3);
}
void save(const char *filename,int m,int n,const float *A,const float *b){
  FILE *file=fopen(filename,"w");
  if(file==NULL){
    puts("ファイルを開けませんでした。");
  }else{
    fwrite(A,sizeof(float),m*n,file);
    fwrite(b,sizeof(float),m,file);
    fclose(file);
  }
}
void save6(const Params6 *params){
  save("fc1.dat",50,784,params->A1,params->b1);
  save("fc2.dat",100,50,params->A2,params->b2);
  save("fc3.dat",10,100,params->A3,params->b3);
}
void load(const char *filename,int m,int n,float *A, float *b){
  FILE *file=fopen(filename,"r");
  if(file==NULL){
    puts("ファイルを開けませんでした。");
  }else{
    fread(A,sizeof(float),m*n,file);
    fread(b,sizeof(float),m,file);
    fclose(file);
  }
}
void load6(Params6 *params){
  load("fc1.dat",50,784,params->A1,params->b1);
  load("fc2.dat",100,50,params->A2,params->b2);
  load("fc3.dat",10,100,params->A3,params->b3);
}
void test(const float *test_x,const unsigned char *test_y,
    int size,int test_count,
    const Params6 *params,
    float *result,float *result_loss){

  int i,sum=0;
  float e=0.0;
  float *y =(float *)malloc(sizeof(float)*10); 

  for(i=0;i<test_count;i++){
    if(inference6(
          params->A1,params->b1,
          params->A2,params->b2,
          params->A3,params->b3,
          test_x+i*size,y) ==test_y[i]){
        sum++;
    }
      e+=loss(y[test_y[i]],1);
  }
  *result=sum*100.0/test_count;
  *result_loss=e/test_count;

  free(y);
}

int main(int argc,char *argv[]) {
  
  Params6 *params=(Params6 *)malloc(sizeof(Params6));
  float *y=(float*)malloc(sizeof(float)*10);
  float *x=load_mnist_bmp(argv[4]);

  load(argv[1],50,784,params->A1,params->b1);
  load(argv[2],100,50,params->A2,params->b2);
  load(argv[3],10,100,params->A3,params->b3);

  printf("answer:%d\n",
    inference6(
      params->A1,params->b1,
      params->A2,params->b2,
      params->A3,params->b3,x,y)
    );
  return 0;
}
