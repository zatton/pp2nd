// author: Kyohei Shimozato
// description: cnn learning program
// Usage example: ./cnn data/data.dat log.txt

#include "nn.h"
#include <time.h>
#include <pthread.h>

#define THREAD 3 //BATCH_SIZE%THREAD must be 0
#define LEARNING_RATE 0.01
#define BATCH_SIZE 100
#define PATIENCE_UNIT 10
#define valid_max 99.5
#define SAVE_FFREQ 5

pthread_mutex_t mutex;

//paramaters structs
typedef struct {
    float w[5 * 5 * 1 * 20];
    float b[20];
} Conv1;
typedef struct {
    float w[5 * 5 * 20 * 50];
    float b[50];
} Conv2;
typedef struct {
    float A[4 * 4 * 50 * 500];
    float b[500];
} Fc1;
typedef struct {
    float A[500 * 10];
    float b[10];
} Fc2;
typedef struct {
    Conv1 conv1;
    Conv2 conv2;
    Fc1 fc1;
    Fc2 fc2;
} P; //all params
typedef struct {
    float conv1[28 * 28];
    float pool1[24 * 24 * 20];
    float relu1[12 * 12 * 20];
    float conv2[12 * 12 * 20];
    float pool2[8 * 8 * 50];
    float relu2[4 * 4 * 50];
    float fc1[4 * 4 * 50];
    float relu3[500];
    float fc2[500];
    float soft[10];
    float y[10];
} X; //inputs and outputs
typedef struct {
    P *p; //paramaters
    float *x; //input
    unsigned char y; //correct answer
    P *dp_ave; //paramaters partials
    int *correct; //correct count (for display accuracy of batch data)
    float *loss_sum; //sum of loss function (for display loss of batch data)
} TP; //thread params
#define for_all(func,p) do{\
    func(5 * 5 * 1 * 20,    p->conv1.w);\
    func(5 * 5 * 20 * 50,   p->conv2.w);\
    func(20,                p->conv1.b);\
    func(50,                p->conv2.b);\
    func(4 * 4 * 50 * 500,  p->fc1.A);\
    func(500,               p->fc1.b);\
    func(10 * 500,          p->fc2.A);\
    func(10,                p->fc2.b);\
  }while(0) //do same operation for all params
void print(int m, int n, const float *x) {

    //x: n*m matrix
    //display x

    int i, j;
    for(i = 0; i < m; i++) {
        for(j = 0; j < n; j++) {
            printf("%.1f ", x[n * i + j]);
        }
        putchar('\n');
    }
}
void copy(int n, const float *x, float *y) {

    //n: x and y size
    //copy x to y

    int i;
    for(i = 0; i < n; i++) {
        y[i] = x[i];
    }
}
void fc(int m, int n, const float *x, const float *A, const float *b, float *y) {

    //m: input size
    //n: output size
    //x: input
    //y: output
    //A: m*n matrix
    //b: n bias
    //y=Ax+b

    int i, j;
    for(i = 0; i < m; i++) {
        y[i] = b[i];
        for(j = 0; j < n; j++) {
            y[i] += A[i * n + j] * x[j];
        }
    }
}
void fc_bwd(int m, int n, const float *x, const float *dy, const float *A, float *dA, float *db, float *dx) {

    //m: input size
    //n: output size
    //x: saved input
    //A: m*n matrix
    //b: n bias
    //dy: output partial
    //dx: input partial

    int i, j;
    for(i = 0; i < m; i++) {
        for(j = 0; j < n; j++) {
            dA[n * i + j] = dy[i] * x[j];
        }
    }

    copy(10, dy, db);

    for(i = 0; i < n; i++) {
        dx[i] = 0;
        for(j = 0; j < m; j++) {
            dx[i] += A[n * j + i] * dy[j];
        }
    }
}
void relu(int n, const float *x, float *y) {

    //n: input and output size
    //x: input
    //y: output
    //remove x<=0

    int i;
    for(i = 0; i < n; i++) {
        if(x[i] > 0) {
            y[i] = x[i];
        } else {
            y[i] = 0;
        }
    }
}
void relu_bwd(int n, const float *x, const float *dy, float *dx) {

    //n: input and output size
    //x: saved input
    //dy: output partial
    //dx: input partial

    int i;
    for(i = 0; i < n; i++) {
        if(x[i] > 0) {
            dx[i] = dy[i];
        } else {
            dx[i] = 0;
        }
    }
}
void pool(int n, int c, const float *x, float *y) {

    //n: input size
    //c: channel number
    //x: n*n input
    //y: n/2*n/2 output

    int i, j, k, l, s;
    for(s = 0; s < c; s++) {
        for(i = 0; i < n / 2; i++) {
            for(j = 0; j < n / 2; j++) {
                y[(n / 2) * (n / 2)*s + i * n / 2 + j] = x[n * n * s + i * n * 2 + j * 2];
                for(k = 0; k < 2; k++) {
                    for(l = 0; l < 2; l++) {
                        if(y[(n / 2) * (n / 2) * s + i * n / 2 + j] < x[n * n * s + (2 * i + k) * n + (2 * j + l)]) {
                            y[(n / 2) * (n / 2) * s + i * n / 2 + j] = x[n * n * s + (2 * i + k) * n + (2 * j + l)];
                        }
                    }
                }
            }
        }
    }
}
void pool_bwd(int n, int c, const float *x, const float *y, const float *dy, float *dx) {

    //n: input size
    //c: channel number
    //x: n*n saved input
    //y: n/2*n/2 saved output
    //dy: output partial
    //dx: input partial

    int i, j, k, l, s;
    for(s = 0; s < c; s++) {
        for(i = 0; i < n / 2; i++) {
            for(j = 0; j < n / 2; j++) {
                for(k = 0; k < 2; k++) {
                    for(l = 0; l < 2; l++) {
                        dx[n * n * s + (2 * i + k)*n + (2 * j + l)] =
                            y[(n / 2) * (n / 2) * s + i * n / 2 + j] == x[n * n * s + (2 * i + k) * n + (2 * j + l)] ?
                            dy[(n / 2) * (n / 2) * s + i * n / 2 + j] : 0;
                    }
                }
            }
        }
    }
}
void conv(int n, int m, int c, int o, const float *x, const float *w, const float *b, float *y) {

    //n: input image size
    //m: filter size
    //c: channel number
    //o: filter number
    //x: n*n*c input
    //w: m*m*o filter
    //b: (n-m+1)*(n-m+1)*o bias
    //y: (n-m+1)*(n-m+1)*o output

    int i, j, k, l, s, t;
    copy((n - m + 1) * (n - m + 1)*o, b, y);
    for(l = 0; l < o; l++) {
        for(i = 0; i < (n - m + 1); i++) {
            for(j = 0; j < (n - m + 1); j++) {
                y[(n - m + 1) * (n - m + 1)*l + i * (n - m + 1) + j] = b[l];
                for(k = 0; k < c; k++) {
                    for(s = 0; s < m; s++) {
                        for(t = 0; t < m; t++) {
                            y[(n - m + 1) * (n - m + 1)*l + i * (n - m + 1) + j] +=
                                w[m * m * (l * c + k) + s * m + t] * x[n * n * k + ((i + s) * n + (j + t))];
                        }
                    }
                }
            }
        }
    }
}
void conv_bwd(int n, int m, int c, int o, const float *x, const float *w, const float *dy, float *dw, float *db, float *dx) {

    //n:input size
    //m:filter size
    //c:channel number
    //o:filter number
    //x:n*n*c input image
    //w:m*m*o filter
    //b:(n-m+1)*(n-m+1)*o bias
    //dy:(n-m+1)*(n-m+1)*o output partial
    //dx: n*n*c input partial

    int i, j, k, l, s, t;
    for(l = 0; l < o; l++) {
        for(k = 0; k < c; k++) {
            for(s = 0; s < m; s++) {
                for(t = 0; t < m; t++) {
                    dw[m * m * (l * c + k) + s * m + t] = 0;
                    for(i = 0; i < n - m + 1; i++) {
                        for(j = 0; j < n - m + 1; j++) {
                            dw[m * m * (l * c + k) + s * m + t] +=
                                dy[(n - m + 1) * (n - m + 1) * l + i * (n - m + 1) + j] * x[n * n * k + (i + s) * n + (j + t)];
                        }
                    }
                }
            }
        }
    }
    for(l = 0; l < o; l++) {
        db[l] = 0;
        for(i = 0; i < n - m + 1; i++) {
            for(j = 0; j < n - m + 1; j++) {
                db[l] += dy[(n - m + 1) * (n - m + 1) * l + i * (n - m + 1) + j];
            }
        }
    }
    for(k = 0; k < c; k++) {
        for(i = 0; i < n; i++) {
            for(j = 0; j < n; j++) {
                dx[n * n * k + i * n + j] = 0;
                for(l = 0; l < o; l++) {
                    for(s = 0; s < m; s++) {
                        for(t = 0; t < m; t++) {
                            if(i - s >= 0 && j - t >= 0) {
                                dx[n * n * k + i * n + j] +=
                                    dy[(n - m + 1) * (n - m + 1) * l + (i - s) * (n - m + 1) + (j - t)] * w[m * m * (l * c + k) + s * m + t];
                            }
                        }
                    }
                }
            }
        }
    }
}
void softmax(int n, const float *x, float *y) {

    //n: input and output size
    //x: n input
    //y: n output

    int i;
    float x_max = 0.0;
    float exp_sum = 0.0;

    for(i = 0; i < n; i++) {
        x_max = x[i] > x_max ? x[i] : x_max;
    }
    for(i = 0; i < n; i++) {
        exp_sum += exp(x[i] - x_max);
    }
    for(i = 0; i < n; i++) {
        y[i] = exp(x[i] - x_max) / exp_sum;
    }
}
void softmaxwithloss_bwd(int n, const float *y, unsigned char t, float *dx) {

    //n: output size
    //y: n result of inference
    //dx: n input partial

    copy(n, y, dx);
    dx[t] -= 1;
}
float loss(const float y, unsigned char t) {

    //loss function

    return -1 * t * log(y + 0.0000001);
}
void shuffle(int n, int *x) {

    //shuffle n size vector x

    int i, j;
    int x_i;
    for(i = 0; i < n; i++) {
        j = (int)(rand() * (float)n / (1.0 + RAND_MAX));
        x_i = x[i]; //一時的に保存
        x[i] = x[j];
        x[j] = x_i;
    }
}
void add(int n, const float *x, float *o) {

    //n: x and o size
    //o = o + x

    int i;
    for(i = 0; i < n; i++) {
        o[i] += x[i];
    }
}
void add_all(const P *p1, P *p2) {

    //add p1 to p2

    add(5 * 5 * 1 * 20,   p1->conv1.w, p2->conv1.w);
    add(5 * 5 * 20 * 50,  p1->conv2.w, p2->conv2.w);
    add(20,               p1->conv1.b,  p2->conv1.b);
    add(50,               p1->conv2.b, p2->conv2.b);
    add(4 * 4 * 50 * 500, p1->fc1.A, p2->fc1.A);
    add(500,              p1->fc1.b, p2->fc1.b);
    add(10 * 500,         p1->fc2.A, p2->fc2.A);
    add(10,               p1->fc2.b, p2->fc2.b);
};
void scale(int n, float x, float *o) {

    //n: x and o size
    //multiple o by x

    int i;
    for(i = 0; i < n; i++) {
        o[i] *= x;
    }
}
//Don't forget to define x before use scale_param
#define scale_param(size,param) do{\
    scale(size,x,param);\
  }while(0)
void scale_p(float x, P *p) {

    //multiple p by x

    for_all(scale_param, p);;
}
void init(int n, float x, float *o) {

    //n: o size
    //x: initial state
    //set each of o to x

    int i;
    for(i = 0; i < n; i++) {
        o[i] = x;
    }
}
void init0(int n, float *o) {

    //n: o size
    //set each of o to 0

    init(n, 0, o);
}
float Uniform() {

    //return 0 to 1 random value

    return ((float)rand() + 1.0) / ((float)RAND_MAX + 2.0);
}
float rand_normal( float mu, float sigma ) {

    //return normal random value

    float z = sqrt( -2.0 * log(Uniform()) ) * sin( 2.0 * M_PI * Uniform() );
    return mu + sigma * z;
}
void rand_init_normal (int n, int m, float *o) {

    //n: o size
    //m: input size of layer = mu
    //initialize using normal random value

    int i;
    for(i = 0; i < n; i++) {
        o[i] = rand_normal(0, sqrt(2.0 / m));
    }
}
void rand_init_all(P *p) {

    //initialize p

    rand_init_normal(5 * 5 * 1 * 20, 28 * 28,       p->conv1.w);
    rand_init_normal(5 * 5 * 20 * 50, 12 * 12 * 20, p->conv2.w);
    rand_init_normal(4 * 4 * 50 * 500, 4 * 4 * 50,  p->fc1.A);
    rand_init_normal(10 * 500, 500,                 p->fc2.A);
    init0(20,  p->conv1.b);
    init0(50,  p->conv2.b);
    init0(500, p->fc1.b);
    init0(10,  p->fc2.b);
}
int inference(const P *p, const float *x_input, X *x) {

    //p: paramaters
    //x_input: 28*28 input
    //x: for saving layer io
    //return answer

    int i, ans = -1;
    float max = 0;

    copy(28 * 28, x_input, x->conv1);

    conv(28, 5, 1, 20, x->conv1, p->conv1.w, p->conv1.b, x->pool1);
    pool(24, 20, x->pool1, x->relu1);
    relu(12 * 12 * 20, x->relu1, x->conv2);

    conv(12, 5, 20, 50, x->conv2, p->conv2.w, p->conv2.b, x->pool2);
    pool(8, 50, x->pool2, x->relu2);

    relu(4 * 4 * 50, x->relu2, x->fc1);

    fc(500, 4 * 4 * 50, x->fc1, p->fc1.A, p->fc1.b, x->relu3);
    relu(500, x->relu3, x->fc2);

    fc(10, 500, x->fc2, p->fc2.A, p->fc2.b, x->soft);
    softmax(10, x->soft, x->y);

    for(i = 0; i < 10; i++) {
        if(x->y[i] > max) {
            max = x->y[i];
            ans = i;
        }
    }
    if(ans == -1) {
        puts("Invalid answer");
        exit(1);
    }
    return ans;
}
int backward(const P *p, const float *x_input, unsigned char t, P *dp, X *x) {

    //p: paramaters
    //x_input: input
    //t: correct answer
    //dp: paramaters partial
    //x: saved io of inference

    X dx;

    int ans = inference(p, x_input, x);

    //backward
    softmaxwithloss_bwd(10, x->y, t, dx.soft);
    fc_bwd(10, 500, x->fc2, dx.soft, p->fc2.A, dp->fc2.A, dp->fc2.b, dx.fc2);

    relu_bwd(500, x->relu3, dx.fc2, dx.relu3);
    fc_bwd(500, 4 * 4 * 50, x->fc1, dx.relu3, p->fc1.A, dp->fc1.A, dp->fc1.b, dx.fc1);

    relu_bwd(4 * 4 * 50, x->relu2, dx.fc1, dx.relu2);
    pool_bwd(8, 50, x->pool2, x->relu2, dx.relu2, dx.pool2);
    conv_bwd(12, 5, 20, 50, x->conv2, p->conv2.w, dx.pool2, dp->conv2.w, dp->conv2.b, dx.conv2);

    relu_bwd(12 * 12 * 20, x->relu1, dx.conv2, dx.relu1);
    pool_bwd(24, 20, x->pool1, x->conv1, dx.relu1, dx.pool1);
    conv_bwd(28, 5, 1, 20, x->conv1, p->conv1.w, dx.pool1, dp->conv1.w, dp->conv1.b, dx.conv1);

    return ans;
}
void test(const float *test_x, unsigned char *test_y, int count,
          const P *p, float *result, float *result_loss) {

    //test_x: set of input
    //test_y: set of correct answer
    //count: test count
    //p: paramaters
    //result: for return
    //result_loss: for return

    int i, correct = 0;
    float e = 0;
    X x;

    for(i = 0; i < count; i++) {
        int res = inference(p, test_x + i * 784, &x);
        if(res == test_y[i]) {
            correct++;
        }
        e += loss(x.y[test_y[i]], 1);
    }
    *result = correct * 100.0 / count;
    *result_loss = e / count;
}
//Don't forget to define *file before use save_param
//for save function
#define save_param(size,param) do{\
    fwrite(param,sizeof(float),size,file);\
  }while(0)
void save(const char *filename, P *p) {

    //p: paramaters
    //save all params to file as binary

    FILE *file = fopen(filename, "w");
    if(file == NULL) {
        printf("Save Error: %s\n", filename);
    } else {
        for_all(save_param, p);
        printf("Saved: %s\n", filename);
        fclose(file);
    }
}
//Don't forget to define *file before use load_param
//for load function
#define load_param(size,param) do{\
    fread(param,sizeof(float),size,file);\
  }while(0)
void load(const char *filename, P *p) {

    //p: paramaters
    //load all params from file

    FILE *file = fopen(filename, "r");
    if(file == NULL) {
        printf("Load Error: %s\n", filename);
    } else {
        for_all(load_param, p);
        fclose(file);
        printf("Loaded: %s\n", filename);
    }
}
void *thread(void *arg) {

    //for thread operation
    //backward -> update params

    TP *tp = (TP *)arg;
    X *x = (X *)malloc(sizeof(X));
    P *dp = (P *)malloc(sizeof(P));

    int res = backward(tp->p, tp->x, tp->y, dp, x);

    pthread_mutex_lock(&mutex);
    add_all(dp, tp->dp_ave);
    if(res == tp->y) {
        (*(tp->correct))++;
    }
    (*(tp->loss_sum)) += loss(x->y[tp->y], 1);
    pthread_mutex_unlock(&mutex);

    free(x);
    free(dp);

    return 0;
}
int main(int argc, char *argv[]) {

    if(argc != 3) {
        puts("Error: Wrong args!");
        return 1;
    }

    float *train_x = NULL;
    unsigned char *train_y = NULL;
    int train_count = -1;
    float *test_x = NULL;
    unsigned char *test_y = NULL;
    int test_count = -1;

    int width = -1;
    int height = -1;

    //load mnist dataaaaaa
    load_mnist(&train_x, &train_y, &train_count,
               &test_x, &test_y, &test_count,
               &width, &height);

    //init rand function
    srand((unsigned int)time(NULL));
    rand();
    rand();
    rand();
    rand();
    rand();

    //create variables
    int i, j, k, l, epoc = 0;
    float result, result_loss;
    float loss_best = 10;
    int patience = PATIENCE_UNIT;
    int batch_count = train_count / BATCH_SIZE;
    int index[train_count];
    int _index;
    int correct = 0;
    float loss_sum = 0;

    //set index
    for(i = 0; i < train_count; i++) {
        index[i] = i;
    }

    //create and initialize params
    P *p       = (P *)malloc(sizeof(P));
    P *dp_ave  = (P *)malloc(sizeof(P));
    rand_init_all(p);

    //create params passed to thread
    pthread_t pt[THREAD];// = (pthread_t *)malloc(sizeof(pthread_t)*THREAD);
    TP tp[THREAD];// = (TP *)malloc(sizeof(TP)*THREAD);
    for(i = 0; i < THREAD; i++) {
        tp[i].p = p;
        tp[i].dp_ave = dp_ave;
        tp[i].correct = &correct;
        tp[i].loss_sum = &loss_sum;
    }

    //load params if exist
    load(argv[1], p);

    //log file
    FILE *log = fopen(argv[2], "a");

    puts("\n*----*TRAINING START*----*\a\a\a");

    while(1) {

        epoc++;
        printf("\nEpoc: %d\n", epoc);

        //prepare for new epoc
        shuffle(train_count, index);

        for(j = 0; j < batch_count; j++) {

            //prepare for new batch
            correct = 0;
            loss_sum = 0;
            for_all(init0, dp_ave);

            for(k = 0; k < BATCH_SIZE / THREAD; k++) {
                for(l = 0; l < THREAD; l++) {

                    //prepare paramss for threads
                    _index = index[j * BATCH_SIZE + k * THREAD + l];
                    tp[l].x = train_x + width * height * _index;
                    tp[l].y = train_y[_index];

                    //create thread
                    pthread_create(&pt[l], NULL, &thread, &tp[l]);
                }
                //wait until all threads end
                for(l = 0; l < THREAD; l++) {
                    pthread_join(pt[l], NULL);
                }
            }

            //update params
            scale_p(-1 * LEARNING_RATE / BATCH_SIZE, dp_ave);
            add_all(dp_ave, p);

            //display train result
            printf("\rbatch: %3d/%d result: %.2f%% loss: %.3f",
                   j + 1, batch_count, correct * 100.0 / BATCH_SIZE, loss_sum / BATCH_SIZE);
            fflush(stdout);

        }
        printf("\r                                         ");

        //test using train data
        printf("\rtesting train data...");
        fflush(stdout);

        test(train_x, train_y, test_count, p, &result, &result_loss);

        printf("\rTRAIN result: %3.2f%% loss: %.3f\n",
               result, result_loss);

        fprintf(log, "epoc: %d train-result: %3.2f%% train-loss: %.3f",
                epoc, result, result_loss);

        //test using test data
        printf("testing test data...");
        fflush(stdout);

        test(test_x, test_y, test_count, p, &result, &result_loss);

        printf("\rTEST  result: %3.2f%% loss: %.3f\n",
               result, result_loss);

        fprintf(log, "test-result: %3.2f%% test-loss: %.3f\n",
                result, result_loss);

        //if test loss is inprove, add patience
        if(loss_best > result_loss) {
            patience = epoc + PATIENCE_UNIT;
            loss_best = result_loss;
            printf("=>patience: %d\n", patience);
        }

        //if epoc reached patience, finish training
        if(epoc >= patience || result >= valid_max) {
            break;
        }

        //save params every specified time
        if(epoc % SAVE_FFREQ == 0) {
            printf("\n");
            save(argv[1], p);
        }
    }

    puts("\n*----*TRAINING END*----*\n\a\a\a");

    //save final params
    printf("\n");
    save(argv[1], p);
    printf("\n");

    fclose(log);

    return 0;
}
