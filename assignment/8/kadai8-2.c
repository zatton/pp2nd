#include<stdio.h>
#include<math.h>
typedef struct{
  double x;
  double y;
} Vector2d;
float getLength(const Vector2d r){
  return sqrt(r.x*r.x+r.y*r.y);
}
int main(void){
  Vector2d r;
  printf("Input 2D Vector: "); scanf("%lf %lf",&r.x,&r.y);
  printf("Length: %.1f\n",getLength(r));
  
  return 0;
}
