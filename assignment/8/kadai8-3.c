#include<stdio.h>
#include<math.h>
typedef struct{
  double x;
  double y;
} Vector2d;
float getLength(const Vector2d r){
  return sqrt(r.x*r.x+r.y*r.y);
}
void scaleVector(Vector2d *r,int n){
  r->x=r->x*n;
  r->y=r->y*n;
}
int main(void){
  Vector2d r;
  double scale;
  printf("Input 2D Vector: "); scanf("%lf %lf",&r.x,&r.y);
  printf("Input scale value: "); scanf("%lf",&scale);

  scaleVector(&r,scale);
  printf("Result: %.1f %.1lf\n",r.x,r.y);
  printf("Length: %.1lf\n",getLength(r));

  return 0;
}
