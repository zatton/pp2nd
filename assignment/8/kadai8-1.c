#include<stdio.h>
void swap(int *a, int *b){
  int tmp=*a;
  *a=*b;
  *b=tmp;
}
void print(int n,const int *a){
  for (int i = 0; i < n; i++) {
    printf("%d ",a[i]);
  }
  putchar('\n');
}
void loop(int n,int *array){
  for (int i = 0; i < n-1; i++) {
    if(array[i]>array[i+1]){
      swap(&array[i],&array[i+1]);
    }
  }
}
void sort(int n,int *array){
  for (int i = 0; i < n-1; i++) {
    loop(n,array);
    printf("loop%d: ",i);
    print(n,array);
  }
}
int main(void){
  int array[6]={64,30,8,87,45,13};
  printf("Data: ");
  print(6,array);
  printf("N: %d\n\n",6);

  sort(6,array);

  
  return 0;
}
