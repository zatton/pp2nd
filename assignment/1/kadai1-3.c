#include<stdio.h>
#include<math.h>
int main(void){
  double a,b,c,ans1,ans2;

  printf("a = "); scanf("%lf",&a);
  printf("b = "); scanf("%lf",&b);
  printf("c = "); scanf("%lf",&c);

  ans1=(-b+sqrt(b*b-4*a*c))/(2*a);
  ans2=(-b-sqrt(b*b-4*a*c))/(2*a);

  printf("%.6f\n",ans1);
  printf("%.6f\n",ans2);

  return 0;
}
