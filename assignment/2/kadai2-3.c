#include<stdio.h>
#include<math.h>
int main(void){
  double a,b,c;

  printf("a = "); scanf("%lf",&a);
  printf("b = "); scanf("%lf",&b);
  printf("c = "); scanf("%lf",&c);

  if(b*b>=4*a*c){
    printf("%.6f\n",(-b+sqrt(b*b-4*a*c))/(2*a));
    printf("%.6f\n",(-b-sqrt(b*b-4*a*c))/(2*a));
  }else{
    printf("%.6f+%.6fi\n",-b/(2*a),sqrt(4*a*c-b*b)/(2*a));
    printf("%.6f-%.6fi\n",-b/(2*a),sqrt(4*a*c-b*b)/(2*a));
  }

  return 0;
}
