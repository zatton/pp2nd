#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 10
void swap(int *a, int *b){
  int tmp=*a;
  *a=*b;
  *b=tmp;
}
void set(int n,int *data){
  for(int i=0; i<n; i++){
    data[i]=(int)(rand()*10000.0/(1.0+RAND_MAX));
  }
}
void show(int n,int *data){
  printf("Data:");
  for(int i=0;i<n;i++){
    printf(" %d",data[i]);
  }
  putchar('\n');
}
void loop(int n,int *array){
  for (int i = 0; i < n-1; i++) {
    if(array[i]>array[i+1]){
      swap(&array[i],&array[i+1]);
    }
  }
}
void sort(int n,int *array){
  for (int i = 0; i < n-1; i++) {
    loop(n,array);
  }
}
int main(void){
  srand((unsigned int)time(NULL));
  rand();rand();rand();rand();rand();

  int data[N];
  printf("N: %d\n",N);
  set(N,data);
  show(N,data);

  sort(N,data);
  show(N,data);
  
  return 0;
}
