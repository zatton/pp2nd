#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#define N 10
void set(int n,int *data){
  for(int i=0; i<n; i++){
    data[i]=(int)(rand()*10000.0/(1.0+RAND_MAX));
  }
}
void show(int n,int *data){
  printf("Data:");
  for(int i=0;i<n;i++){
    printf(" %d",data[i]);
  }
  putchar('\n');
}
int main(void){
  srand((unsigned int)time(NULL));
  rand();rand();rand();rand();rand();

  int data[N];
  printf("N: %d\n",N);
  set(N,data);
  show(N,data);

  return 0;
}
