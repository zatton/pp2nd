#include<stdio.h>
#include<stdlib.h>
#include<time.h>
void swap(int *a, int *b){
  int tmp=*a;
  *a=*b;
  *b=tmp;
}
void set(int n,int *data){
  for(int i=0; i<n; i++){
    data[i]=(int)(rand()*10000.0/(1.0+RAND_MAX));
  }
}
void show(int n,int *data){
  printf("Data:");
  for(int i=0;i<n;i++){
    printf(" %d",data[i]);
  }
  putchar('\n');
}
void loop(int n,int *array){
  for (int i = 0; i < n-1; i++) {
    if(array[i]>array[i+1]){
      swap(&array[i],&array[i+1]);
    }
  }
}
void sort(int n,int *array){
  for (int i = 0; i < n-1; i++) {
    loop(n,array);
  }
}
void save(int n,int *data,char *filename){
  FILE *file=fopen(filename,"w");
  if(file==NULL){
    puts("File output: Error");
  }else{
    fprintf(file,"Data: ");
    for(int i=0;i<n;i++){
      fprintf(file," %d",data[i]);
    }
    fclose(file);
    puts("File output: OK");
  }
}
int main(int argc, char *argv[]){
  srand((unsigned int)time(NULL));
  rand();rand();rand();rand();rand();

  int n=0;
  char *filename="";
  if(argc>0){
    n=atoi(argv[1]);
    filename=argv[2];
  }
  if(n==0){
    puts("Wrong Argument");
    return 0;
  }

  int *data=malloc(sizeof(int)*n);
  printf("N: %d\n",n);
  set(n,data);
  sort(n,data);
  save(n,data,filename);
  
  return 0;
}
