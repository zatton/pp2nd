#include<stdio.h>
#include <stdlib.h>
#include <time.h>
void print_oct(int m,int n, const float *x,const char *name){
  int i,j;
  printf("%s = [",name);
  for(i=0;i<m;i++){
    for(j=0;j<n;j++){
      printf(" %.4f ",x[n*i+j]);
    }
    printf(";\n");
  }
  printf("];\n");
}
void rand_init(int n,float *o){
  int i;
  for(i=0;i<n;i++){
    o[i]=-1.0+rand()*2.0/(1.0+RAND_MAX);
  }
}
int main(void){
  srand((unsigned int)time(NULL));
  rand();rand();rand();rand();rand();

  float y[6];
  print_oct(2,3,y,"y");
  rand_init(6,y);
  print_oct(2,3,y,"y");
  
  return 0;
}
