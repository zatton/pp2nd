#include<stdio.h>
#include <stdlib.h>
#include<time.h>

int input_hand(){
  int input;
  while(1){
    printf("Your input (0,2,5):"); scanf("%d",&input);
    if(input!=0&&input!=2&&input!=5){
      printf("Invalid input => Input again.\n");
      continue;
    }else{
      break;
    }
  }
  return input;
}

int rand_hand(){
  int comp;
  switch(rand()%3){
    case 0:
      comp=0;
      break;
    case 1:
      comp=2;
      break;
    case 2:
      comp=5;
      break;
  }
  return comp;

}

int main(void){
  int input,comp;
  //rand初期化
  srand((unsigned int)time(NULL));
  rand();rand();rand();rand();rand();

  while(1){

    input=input_hand();
    comp=rand_hand();
  
    printf("Comp:%d vs You:%d",comp,input);
  
    if(input==comp){
      //あいこ
      printf(" => Try again.\n");
      continue;
    }
    else if((input==0&&comp==2)||(input==2&&comp==5)||(input==5&&comp==0)){
      //inputが勝つ
      printf(" => You win.\n");
      break;
    }else{
      //compが勝つ
      printf(" => You lose.\n");
      break;
    }
    
  }
  return 0;
}
