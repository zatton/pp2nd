#include<stdio.h>

int input(){
  int n,r;
  while(1){
    printf("n = "); 
    r=scanf("%d",&n);

    if(n<0||r!=1){
      scanf("%*s");
      puts("Invalid input\n");
      continue;
    }else{
      break;
    }
  }

  return n;
}

int factorial(int n){
  int i,res=1;
  if(n>0){
    for(i=1;i<=n;i++){
      res*=i;
    }
  }
  return res;
}

int perm(int n, int r){
  return factorial(n)/factorial(n-r);
}

int main(void){
  int r;
  int n;

  n=input();

  for(r=0;r<n;r++){
    printf("perm(%d,%d) = %d\n",n,r,perm(n,r));
  }
  
  return 0;
}
