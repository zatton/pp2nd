#define M 3
#define N 4

#include<stdio.h>

int a[M][N];

void input(){
  int i,j;
  for(i=0;i<M;i++){
    for(j=0;j<N;j++){
      a[i][j]=10*i+j;
    }
  }
}

void print(){
  int i,j;
  for(i=0;i<M;i++){
    for(j=0;j<N;j++){
      printf("%3d",a[i][j]);
    }
    putchar('\n');
  }
}

int main(void){
  input();
  print();
  
  return 0;
}
